<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Buyer;
use Illuminate\Http\Request;

class BuyerProductsController extends ApiController
{
    public function index(Buyer $buyer)
    {
        // because buyer who did transaction on that product should appear!

        $products = $buyer->transactions()
                            ->with('product')
                            ->get()
                            ->pluck('product');

        return $this->showAll($products);

    }
}
