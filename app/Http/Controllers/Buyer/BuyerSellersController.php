<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Buyer;
use Illuminate\Http\Request;

class BuyerSellersController extends ApiController
{
    public function index(Buyer $buyer){

        $sellers = $buyer->transactions()
                    ->with('product.seller')
                    ->get()
                    ->pluck('product.seller');

        return $this->showAll($sellers);

    }
}
