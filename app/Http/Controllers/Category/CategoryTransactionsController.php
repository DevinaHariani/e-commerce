<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryTransactionsController extends ApiController
{
    public function index(Category $category)
    {
        $transactions = $category->products()
        ->whereHas('transactions')
        ->get()
        ->pluck('transactions');

        return $this->showAll($transactions);
        
    }
}
