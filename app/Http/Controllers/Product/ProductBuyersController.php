<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductBuyersController extends ApiController
{
    public function index(Product $product){
        $buyers= $product
        ->with('transactions.buyer')
        ->get()
        ->pluck('transactions')
        ->flatten()
        ->pluck('buyer')
        ->flatten()
        ->unique()
        ->values();
        return $this->showAll($buyers);
    }
}
