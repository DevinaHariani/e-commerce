<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Seller;
use Illuminate\Http\Request;

class SellerBuyersController extends ApiController
{
    public function index(Seller $seller)
    {
        $transactions = $seller->products()
        ->with('transactions.buyer')
        ->get()
        ->pluck('transactions')
        ->flatten()
        ->pluck('buyer')
        ->flatten()
        ->unique()
        ->values();

        return $this->showAll($transactions);
    }
}
