<?php

namespace App\Models;

use App\Models\Scopes\BuyerScope;
use Illuminate\Database\Eloquent\Attributes\ScopedBy;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User;


#[ScopedBy(BuyerScope::class)]
class Buyer extends User
{
    use HasFactory,SoftDeletes;
    protected $table = 'users';

   public function transactions(): HasMany {
    return $this->hasMany(Transaction::class);
   }

}
