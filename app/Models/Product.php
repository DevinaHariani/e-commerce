<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    const UNAVAILABLE_PRODUCT = 'unavailable';
    const AVAILABLE_PRODUCT = 'available';

    public function categories(): BelongsToMany {
        return $this->belongsToMany(Category::class);
    }
    public function transactions(): HasMany {
        return $this->hasMany(Transaction::class);
    }
    public function seller() : BelongsTo {
        return $this->belongsTo(Seller::class);
    }
}
