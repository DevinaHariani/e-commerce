<?php

namespace App\Models;

use App\Models\Scopes\SellerScope;
use Illuminate\Database\Eloquent\Attributes\ScopedBy;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

#[ScopedBy(SellerScope::class)]
class Seller extends User
{
    use HasFactory, SoftDeletes;
    protected $table = 'users';

    public function products() : HasMany{
        return $this->hasMany(Product::class);
    }
}
