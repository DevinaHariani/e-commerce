<?php

namespace Database\Seeders;

use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // User::factory(10)->create();

        User::factory()->create([
            'name' => 'Test User',
            'email' => 'test@example.com',
        ]);

        // only perform when not on production , it will truncate and fill the migration!

        if(App::environment() === 'production') exit();

        $tables = DB::select("SHOW TABLES");
        $key = "Tables_in_" . env('DB_DATABASE');

        // this removes foreign key constraint from entire db! remember to turn it on afterwards! importnat!
        DB::statement("SET FOREIGN_KEY_CHECKS =0");
        foreach($tables as $table){
            if($table->$key !== 'migrations'){
                DB::table($table->$key)->truncate();
            }
        }
        $this->call([
            UserSeeder::class,
            CategorySeeder::class,
            ProductSeeder::class,
            TransactionSeeder::class
        ]);

        DB::statement("SET FOREIGN_KEY_CHECKS =1");
    }
}
