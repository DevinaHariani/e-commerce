<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // important product ko vohi categories milni chaiye jo important hai.

        Product::factory(100)
        ->create()
        ->each(function($product){
            $categories = Category::all()->random(random_int(1,5))->pluck('id');
            $product->categories()->attach($categories);
        });
    }
}
